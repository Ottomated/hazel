var assert = require('assert');

// Describes the program
describe('Program', function () {
  // Lets you access the other code
  program = require('../code');
  // Interior part of the program

  describe('multiply()', function () {
    it('should multiply two numbers', function () {

      assert.equal(program.multiply(4, 5), 20);
      
    });
  });
  describe('add()', function () {
    it('should add two numbers', function () {

      assert.equal(program.add(4, 5), 9);
      
    });
  });
  describe('age()', function () {
    it('should determine age', function () {

      assert.equal(program.age(25), "young");
      
    });
  });
});