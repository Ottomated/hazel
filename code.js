
// This is a comment

// Always use comments, they help you understand what your code is doing

// Whenever you do something important, push it

// Do: "git add ." <-- Adds all the files to the new thing you're pushing

// "git commit -m 'Message'" <-- Sets up the new thing (commit)

// "git push origin master" <-- Puts it online

// This allows you to backtrack if you want, and everyone does it

function multiply(n1, n2) {
    return n1 * n2;
}

function add(n1, n2) {
    return n1 + n2;
}

function age(number) {
    if (number > 60) {
        return "Very old";
    } else if (number > 40) {
        return "Old";
    } else if (number > 20) {
        return "young";
    } else {
        return "super young";
    }
}


module.exports = {
    multiply: multiply,
    add: add,
    age: age
};